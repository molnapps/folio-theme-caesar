<div class="Section text-center py-16">
	<div class="mx-auto md:w-4/5 xl:w-2/3">
		<div class="flex justify-center">
			<h1 class="text-3xl pb-8 text-red-500 px-4 animate-words animate-words--slide-up">Selected Awards</h1>
		</div>
		<ul class="flex flex-wrap">
			@foreach (registry()->awards()->all() as $award)
			<li class="About__card About__award w-full md:w-1/3 py-16 px-4 flex flex-col items-center">
				<span class="w-1/3 flex justify-center items-center mb-8">
					<img src="{{ $award['image'] }}" width="100" height="100">
				</span>
				<p class="flex flex-col">
					<span>{{ $award['competition'] }}</span>
					<strong class="block">{{ $award['award'] }}</strong>
					<span>{{ $award['project'] }}</span>
					<span class="text-gray-500 leading-none">
						{{ $award['category'] }}
					</span>
				</p>
			</li>
			@endforeach
		</ul>
	</div>
</div>