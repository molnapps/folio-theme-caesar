let mix = require('laravel-mix');

mix.setPublicPath('assets')
mix.js('resources/js/template.js', '');
mix.postCss('resources/scss/template.css', '', [
	require('tailwindcss'),
]);
mix.sass('resources/scss/swiper.scss', '');