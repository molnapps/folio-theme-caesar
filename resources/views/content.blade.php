<!-- Hero -->
@if ($post->hero->exists())
	<div class="relative overflow-hidden" style="height: 66vh;">
		{!! $post->hero->getHtml([
			'class' => 'parallax h-full w-auto min-w-full object-cover'
		]) !!}
	</div>
@endif
<!-- Hero Vimeo -->
@if ($post->hero_vimeo)
	<div class="relative overflow-hidden" style="height: 66vh;">
		<div 
			class="Hero__vimeo parallax h-full w-auto min-w-full object-cover" 
			id="vimeo-hero"
			data-vimeo-defer
			data-vimeo-id="{{ $post->hero_vimeo }}"
		></div>
	</div>
@endif
<!-- Hero Text -->
<div class="bg-white py-8 px-4 md:px-0">
	<div class="md:flex md:w-4/5 xl:w-2/3 md:mx-auto">
		<div class="flex-1 text-3xl leading-tight pr-8">
			<h1 class="p-0 m-0">{{ $post->title }}</h1>
			<h2 class="text-red-500 p-0 m-0">{{ $post->client }}</h2>
			<p class="text-gray-500 p-0 m-0">{{ $post->excerpt }}</p>
		</div>
		<div class="mt-4 md:mt-0 flex-1 text-gray-600">
			{!! $post->html()->getLead() !!}
		</div>
	</div>
</div>
<!-- Body -->
<div class="Post Post--{{ $post->slug }}">
	<div class="md:w-4/5 xl:w-2/3 md:mx-auto py-16 border-b border-gray-200">
		{!! $post->html()->getBody() !!}
		@if ($post->credits)
			<div class="Credits">
				<h3>Credits</h3>
				{!! $post->html()->getCredits() !!}
			</div>
		@endif
		@if ($post->type != 'page')
			<h4>&copy; {{ $post->getDate()->format('Y') }}</h4>
		@endif
	</div>
</div>