<!-- Icon -->
<link rel="icon" href="{{ image('_icons/icon-pos-150x150.png') }}?w=32" sizes="32x32"/>
<link rel="icon" href="{{ image('_icons/icon-150x150.png') }}?w=192" sizes="192x192"/>
<link rel="shortcut icon" href="{{ image('_icons/icon-150x150.png') }}?w=150" sizes="150x150">
<link rel="apple-touch-icon" href="{{ image('_icons/icon-150x150.png') }}">
<link rel="image_src" href="{{ image('_icons/icon-150x150.png') }}"> 
<meta name="msapplication-TileImage" content="{{ image('_icons/icon-150x150.png') }}?w=270"/>
<!-- Facebook Meta Tags -->
<meta property="og:url" content="{{ $post->getOpenGraph()->getUrl() }}">
<meta property="og:type" content="{{ $post->getOpenGraph()->getType() }}">
<meta property="og:title" content="{{ $post->getOpenGraph()->getTitle() }}">
<meta property="og:description" content="{{ $post->getOpenGraph()->getDescription() }}">
<meta property="og:image" content="{{ $post->getOpenGraph()->getImageUrl() }}">
<meta property="og:image:secure_url" content="{{ $post->getOpenGraph()->getImageUrl() }}">
<!-- Twitter Meta Tags -->
<meta name="twitter:card" content="summary_large_image">
<meta property="twitter:domain" content="{{ env('BASE_URL') }}">
<meta property="twitter:url" content="{{ $post->getOpenGraph()->getUrl() }}">
<meta name="twitter:title" content="{{ $post->getOpenGraph()->getTitle() }}">
<meta name="twitter:description" content="{{ $post->getOpenGraph()->getDescription() }}">
<meta name="twitter:image" content="{{ $post->getOpenGraph()->getImageUrl() }}">