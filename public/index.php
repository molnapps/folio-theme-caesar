<?php

require_once "../vendor/autoload.php";

use App\Portfolio;
use Folio\Themes\Caesar\BootstrapHelper;

BootstrapHelper::bootstrap(__DIR__ . '/..');

echo app(Portfolio::class)->run();