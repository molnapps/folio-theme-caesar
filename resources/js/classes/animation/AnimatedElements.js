export default class AnimatedElements 
{
	constructor (selector) {
		this.selector = selector
	}

	create (animation) {
		document
			.querySelectorAll(this.selector)
			.forEach((element, i) => {
				new animation(element, i).create()
			})
	}
}