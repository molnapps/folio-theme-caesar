<?php

namespace Folio\Themes\Caesar;

use App\Domain\Post;

class ViewHelper
{
	public static function dataJsPreviews(Post $post)
	{
		return (new static)->getDataJsPreviews($post);
	}

	public function getDataJsPreviews(Post $post)
    {
        return json_encode(
			array_map(
				function ($preview) {
					return [
						'src' => $preview['resource']['src'],
						'type' => $preview['type'],
						'media' => $preview['media']
					];
				},
				$post->previews->filterByMedia('preview')->apiResource()->toArray()
			)
        );
    }
}