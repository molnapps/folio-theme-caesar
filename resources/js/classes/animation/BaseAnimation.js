import anime from 'animejs/lib/anime.es.js';

export default class BaseAnimation
{
	constructor (root, selector, override) {
		this.root = root
		this.selector = selector
		this.override = override
	}

	animate(effect) {
		let base = {
			targets: this.root.querySelectorAll(this.selector),
			easing: 'easeInOutQuad'
		}

		anime(
			Object.assign(base, effect, this.override)
		)
	}
}