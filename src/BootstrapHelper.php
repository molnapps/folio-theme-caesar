<?php

namespace Folio\Themes\Caesar;

use App\App;
use App\Portfolio;
use Dotenv\Dotenv;
use App\Views\OpenGraph\OpenGraph;
use Folio\Contents\TestContentProvider;
use App\Repositories\FileSystem\Registry;
use App\Repositories\Json\JsonRepository;

class BootstrapHelper
{
    public static function bootstrap(string $root)
    {
        return (new static)->bootstrapSession($root);
    }

    private function bootstrapSession(string $root)
    {
        $this->bootstrapDotEnv($root);
        $this->bootstrapRegistry();
        $this->bootstrapThemes();
        $this->bootstrapSettings();
        $this->bootstrapOpenGraph();
        $this->bootstrapHomepage();
        $this->bootstrapAboutHero();
        $this->registerAboutRepositories();
        $this->bootstrapClientsRegistry();
    }

    private function bootstrapDotEnv(string $root)
    {
        $dotenv = Dotenv::createImmutable($root);
        $dotenv->load();
    }

    private function bootstrapRegistry()
    {
        App::bootstrapRegistry(
            new Registry(
                (new TestContentProvider)->getBasePath()
            )
        );
    }

    private function bootstrapThemes()
    {
        App::bootstrapThemes([
            'caesar' => ThemeProvider::class
        ]);
    }

    private function bootstrapSettings()
    {
        registry()
            ->settings()
            ->merge([
                'theme' => 'caesar',
                'title' => 'Caesar Theme',
            ]);
    }

	private function bootstrapOpenGraph()
	{
		App::bootstrapOpenGraph(
            function ($settings) {
                return [
                    'title' => $settings->company,
                    'description' => $settings->fullName . ' / ' . $settings->jobTitle,
                    'image' => '_icons/website-feature.png',
                ];
            }
        );
	}

    private function bootstrapHomepage()
    {
        registry()
            ->settings()
            ->merge([
                'defaultSlug' => 'welcome'
            ]);

        app(Portfolio::class)
            ->pages()
            ->addWithArray([
                'type' => 'page', 
                'slug' => 'welcome', 
                'previews' => [
                    [
                        'src' => 'https://loremflickr.com/1920/1080', 
                        'type' => 'image', 
                        'media' => ['hero']
                    ]
                ]
            ]);
    }

    private function bootstrapAboutHero()
    {
        registry()
            ->settings()
            ->merge([
                'photographer' => 'Jane Doe',
                'portrait_bg_desktop' => '~/_assets/caesar/images/portrait-bg-desktop.png',
                'portrait_body_desktop' => '~/_assets/caesar/images/portrait-body-desktop.png',
                'portrait_bg_mobile' => '~/_assets/caesar/images/portrait-bg-mobile.png',
                'portrait_body_mobile' => '~/_assets/caesar/images/portrait-body-mobile.png',
            ]);
    }

    private function registerAboutRepositories()
    {
        foreach (['publicity', 'brands', 'awards'] as $repository) {
            registry()->register(
                $repository, 
                new JsonRepository(
                    "{$_ENV['BASE_PATH_JSON']}/{$repository}.json"
                )
            );
        }
    }

    private function bootstrapClientsRegistry()
    {
        registry()
            ->clients()
            ->merge([
                $this->getDummyClient()
            ]);
    }

    private function getDummyClient()
    {
        return [
            'slug' => 'Bar', 
            'title' => 'Bar', 
            'logo' => 'https://loremflickr.com/50/50'
        ];
    }
}