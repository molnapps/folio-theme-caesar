export default class WordWrapper
{
	constructor(selector) {
		this.elements = document.querySelectorAll(selector)
	}

	create(className) {
		for (var i = 0; i < this.elements.length; i++) {
			var words = this.elements[i].innerText.split(' ')
			this.elements[i].innerText = ''
			
			for (var k = 0; k < words.length; k++) {
				var word = document.createElement('span')
				word.className = className
				word.innerText = words[k]
				this.elements[i].appendChild(word)
				this.elements[i].appendChild(document.createTextNode(' '))
			}
		}
	}
}