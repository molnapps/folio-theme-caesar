<div class="py-16">
	<div class="mx-auto md:w-4/5 xl:w-2/3">
		<!-- Title -->
		<div class="flex justify-center">
			<h1 class="text-3xl pb-8 text-red-500 px-4 text-center animate-words animate-words--slide-up">Selected Works</h1>
		</div>
		<!-- Gallery -->
		<div data-js="projects">
			<!-- Masonry -->
			<div class="masonry-grid-sizer"></div>
			<div class="masonry-gutter-sizer"></div>
			<!-- Project -->
			@foreach ($projects as $post)
				<div 
					class="masonry-grid-item" 
					data-js="project" 
					data-js-previews="{{ \Folio\Themes\Caesar\ViewHelper::dataJsPreviews($post) }}"
				>
					<div class="image-box">
						@if ($post->preview)
							<div data-js="project-preview" class="preview preview--default">
								{!! $post->preview->getHtml([
									'alt' => $post->title, 
									'class' => 'picture image-box-animate-slow',
								]) !!}
							</div>
						@endif
						<div class="image-box-info image-box-animate-slow">
							<div class="image-box-animate logo">
								{!! $post->client->logo->normalizeSize(2000)->getHtml([
									'alt' => $post->client->title
								]) !!}
							</div>
							<div class="details">
								<h3 class="image-box-animate title"><strong>{{ $post->title }}</strong></h3>
								<span class="image-box-animate client">{{ $post->client }}</span>
							</div>
							<p class="image-box-animate-slow description">{{ $post->excerpt }}</p>
							<a href="{{ $context->getPath($post) }}" class="link">
								<span>View Project</span>
							</a>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
</div>