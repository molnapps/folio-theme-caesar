import BaseAnimation from './BaseAnimation'

export default class SlideDown extends BaseAnimation
{
	create() {
		this.animate({ translateY: ['-100%', '0%'] })
	}
}