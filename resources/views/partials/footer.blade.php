<footer>
	<div class="px-4 md:px-0 md:w-4/5 xl:w-2/3 mx-auto flex flex-col md:flex-row justify-between pt-4 pb-24 border-t border-gray-200">
		<span>
			<strong>{{ $settings->fullName }}</strong>
			<span class="text-red-500">{{ $settings->jobTitle }}</span>
		</span>
		<span><span class="text-red-500">&copy;</span> 2007 <span class="text-red-500">—</span> {{ $utilities->year }}</span>
	</div>
</footer>