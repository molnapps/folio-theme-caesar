window.addEventListener('DOMContentLoaded', (event) => {
	const parallaxElements = document.querySelectorAll('.parallax')
	const headerElement = document.querySelector('[data-js="header"]')
	const headerHeight = headerElement.getBoundingClientRect().height

	const parallax = (elements) => {
		if ('undefined' === elements) {
			return
		}

		elements.forEach((element) => {
			let y = - (element.getBoundingClientRect().top - headerHeight) * 0.5
			element.style.transform = `translate3d(0, ${y}px, 0)`
		})
	}

	parallax(parallaxElements)

	window.onscroll = () => {
		parallax(parallaxElements)
	}
})