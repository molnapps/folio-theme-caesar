@extends('layout')
@section('contents')
	@php
	/*
	<div class="relative overflow-hidden">
		<div class="parallax">
				<div 
					class="Hero__vimeo" 
					style="height: 66vh;" 
					id="vimeo-hero"
					data-vimeo-defer
					data-vimeo-id="{{ $settings->hero_vimeo_id }}"
				></div>
				<div class="absolute inset-0 z-80 flex flex-col justify-center items-center bg-black bg-opacity-50 text-white text-2xl md:text-5xl text-center">
					<span 
						class="animate-words animate-words--slide-up"
					>{{ $settings->fullName }}</span>
					<span 
						class="text-red-500 animate-words animate-words--slide-down"
					>{{ $settings->jobTitle }}</span>
				</div>	
		</div>
	</div>
	*/
	@endphp

	@if ($post->hero->exists())
	<!-- Hero -->
	<div class="relative overflow-hidden" style="height: 66vh;">
		{!! $post->hero->getHtml([
			'class' => 'parallax h-full w-auto min-w-full object-cover'
		]) !!}
		<div class="absolute inset-0 z-80 flex flex-col justify-center items-center bg-black bg-opacity-20 text-white text-2xl md:text-5xl text-center">
			<span 
				class="animate-words animate-words--slide-up"
			>{{ $settings->fullName }}</span>
			<span 
				class="text-red-500 animate-words animate-words--slide-down"
			>{{ $settings->jobTitle }}</span>
		</div>	
	</div>
	@endif
	@include('gallery')
@endsection