<style>
@font-face {
	font-family: 'Neue Machina Light';
	src:  url('{{ theme()->assets('fonts/neue-machina/NeueMachina-Light.woff2') }}') format('woff2'),
		url('{{ theme()->assets('fonts/neue-machina/NeueMachina-Light.woff') }}') format('woff');
}
@font-face {
	font-family: 'Neue Machina Regular';
	src:  url('{{ theme()->assets('fonts/neue-machina/NeueMachina-Regular.woff2') }}') format('woff2'),
		url('{{ theme()->assets('fonts/neue-machina/NeueMachina-Regular.woff') }}') format('woff');
}
</style>