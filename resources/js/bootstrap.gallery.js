import Gallery from 'folio-rich-gallery/src/js/Gallery.js'

window.addEventListener('DOMContentLoaded', (event) => {
    if ( ! document.querySelector('[data-js="projects"]')) {
        return;
    }

    new Gallery({
        selector: '[data-js="projects"]',
        itemSelector: '[data-js="project"]',
        masonry: {
            itemSelector: '.masonry-grid-item',
            columnWidth: '.masonry-grid-sizer',
            gutter: '.masonry-gutter-sizer',
        },
        previews: {
            itemSelector: '[data-js="project-preview"]',
            formats: ['square', 'portrait']
        }
    }).attach()
})