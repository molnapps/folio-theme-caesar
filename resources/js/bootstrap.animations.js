import WordWrapper from './classes/WordWrapper'
import Mask from './classes/Mask'
import AnimatedElements from './classes/animation/AnimatedElements'
import { WordsSlideUp, WordsSlideDown } from './classes/animation/all'
import ProjectAnimation from './classes/animated-components/ProjectAnimation'

document.addEventListener('DOMContentLoaded', (event) => {
	new WordWrapper('.animate-words').create('word')
	new Mask('.word').inline().create()

	new Mask('.apply-mask').create()

	new AnimatedElements('.animate-words--slide-up').create(WordsSlideUp)
	new AnimatedElements('.animate-words--slide-down').create(WordsSlideDown)

	new AnimatedElements('.Gallery__project').create(ProjectAnimation)
})