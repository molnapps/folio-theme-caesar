@extends('layout')
@section('contents')
	@include('about.hero')
	@include('about.brands')
	@include('about.awards')
	@include('about.publicity')
@endsection