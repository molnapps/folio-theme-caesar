<div class="Section text-center py-16">
	<div class="mx-auto md:w-4/5 xl:w-2/3">
		<div class="flex justify-center">
			<h1 class="text-3xl pb-8 text-red-500 px-4 animate-words animate-words--slide-up">
				Selected Brands
			</h1>
		</div>
		<div class="text-center grid grid-cols-1 md:grid-cols-3 gap-4">
			{!! app(\App\Views\ClientsGrid::class)
				->withWrapper(
					'<span class="block h-32 w-full flex justify-center items-center">%s</span>'
				)
				->getHtml() !!}
		</div>
	</div>
</div>