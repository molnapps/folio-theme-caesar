<div class="About__hero bg-cover relative" style="background-image: url({{ $settings->portrait_bg_desktop  }});">
	<!-- Hero -->
	<div class="mx-auto px-4 md:px-0 md:w-4/5 xl:w-2/3 flex flex-col md:flex-row-reverse items-center overflow-hidden">
		<img 
			src="{{ $settings->portrait_body_desktop }}" 
			width="2132" 
			height="2132" 
			class="hidden xl:block w-1/2" 
		/>
		<div class="px-4 md:px-0 lg:w-1/2 xl:hidden">
			<div class="w-full mt-8 lg:mt-0 mx-auto bg-cover" style="clip-path: circle(50%); background-image: url({{ $settings->portrait_bg_mobile  }});">
				<img src="{{ $settings->portrait_body_mobile }}" width="2132" height="2132" />
			</div>
		</div>
		<div class="lg:w-1/2 flex flex-col justify-end">
			<p class="text-5xl text-red-500 mt-4 md:mt-16 mb-8">
				{!! str_replace(' ' , '<br/>', $post->greetings) !!}
			</p>
			<div class="About__body mb-8">
				{!! $post->html()->getBody() !!}
			</div>
			<div class="flex flex-col xl:flex-row items-center md:items-start xl:items-center mb-8">
				<ul class="About__socials flex mb-4 xl:mb-0">
					@foreach ($links as $link)
						<li class="About__social mr-2">
							<a href="{{ $link->url }}" class="block">
								<img 
									src="/_assets/caesar/images/social/{{ $link->slug }}.svg" 
									alt="{{ $link->text }}" 
									width="40" 
									height="40" 
								/>
							</a>
						</li>
					@endforeach
				</ul>
				<p class="About__photo xl:flex flex-col text-xs xl:ml-8 text-white">
					<span>photo courtesy of</span>
					<strong>{{ $settings->photographer }}</strong>
				</p>
			</div>
		</div>
	</div>
</div>