<div class="bg-red-200 relative">
	<div class="vimeo-wrapper">
		<iframe 
			src="https://player.vimeo.com/video/76979871?background=1&autoplay=1&loop=1&byline=0&title=0"
	       frameborder="0" 
	       webkitallowfullscreen 
	       mozallowfullscreen 
	       allowfullscreen
	   	></iframe>
	</div>
	<div class="absolute top-0 bottom-0 left-0 right-0 z-80 flex flex-col justify-center items-center bg-black bg-opacity-50 text-white text-5xl">
		<span>{{ $settings->fullName }}</span>
		<span class="text-red-500">{{ $settings->jobTitle }}</span>
	</div>
</div>