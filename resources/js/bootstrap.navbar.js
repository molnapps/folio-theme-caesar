window.addEventListener('DOMContentLoaded', (event) => {
    const app = {
        toggables: {
            menu: {
                isOpen: false,
                button: document.querySelector('[data-js="mobile-menu-button"]'),
                container: document.querySelector('[data-js="mobile-menu"]')
            },
            contact: {
                isOpen: false,
                button: document.querySelector('[data-js="contact-button"]'),
                container: document.querySelector('[data-js="contact"]')
            }
        }
    }
    
    app.toggables.menu.button.addEventListener("click", () => {
        toggleSync(app.toggables.menu, [app.toggables.contact]);
    });

    app.toggables.contact.button.addEventListener("click", (e) => {
        e.preventDefault();
        
        toggleSync(app.toggables.contact, [app.toggables.menu]);
    });

    function toggleSync (masterToggable, slaveToggables) {
        masterToggable.isOpen = ! masterToggable.isOpen;

        slaveToggables.forEach((slaveToggable) => {
            slaveToggable.isOpen = masterToggable.isOpen;
        })

        toggle(masterToggable)
        
        slaveToggables.forEach((slaveToggable) => {
            toggle(slaveToggable);
        })
    }

    function toggle(toggable) {
        toggleContainerVisibility(toggable)
        toggleButtonText(toggable)
    }

    function toggleContainerVisibility(toggable) {
        if (toggable.isOpen) {
            toggable.container.classList.remove('hidden');
        } else {
            toggable.container.classList.add('hidden');
        }
    }

    function toggleButtonText(toggable) {
        if ( ! toggable.button.getAttribute('data-js-open')) {
            return;
        }
        
        if (toggable.isOpen) {
            toggable.button.innerText = toggable.button.getAttribute('data-js-close');
        } else {
            toggable.button.innerText = toggable.button.getAttribute('data-js-open');
        }
    }
})