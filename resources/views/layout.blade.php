<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>{{ $settings->title }}</title>
	@include('partials.fonts')
	<link rel="stylesheet" href="{{ theme()->assets('template.css') }}">
	<link rel="stylesheet" href="{{ theme()->assets('swiper.css') }}">
	@include('partials.icons')
</head>
<body>
	@include('partials.navbar')
	@yield('contents')
	@include('partials.footer')
	<script src="{{ theme()->assets('template.js') }}"></script>
</body>
</html>