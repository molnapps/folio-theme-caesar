import BaseWordsAnimation from './BaseWordsAnimation'
import SlideDown from './SlideDown'

export default class WordsSlideDown extends BaseWordsAnimation
{
	getTransition() {
		return SlideDown
	}
}