module.exports = {
  purge: [
    './resources/**/*.blade.php',
    './resources/**/*.js',
    './resources/**/*.scss',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      screens: {
        'xl': '1920px'
      },
      colors: {
        red: {
          500: '#ff3300'
        }
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}