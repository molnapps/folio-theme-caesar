import Player from '@vimeo/player';

document.addEventListener('DOMContentLoaded', (event) => {
	if ( ! document.getElementById('vimeo-hero')) {
		return
	}

	const player = new Player('vimeo-hero', {
        background: true,
        autoplay: true,
        loop: true,
        byline: false,
        title: false
    });
    
    player.on('loaded', () => {
    	const iframe = document.querySelector('#vimeo-hero iframe')
    	iframe.style.opacity = 0
    	iframe.style.transition = 'opacity 1s'
    })

    player.on('play', () => {
    	const iframe = document.querySelector('#vimeo-hero iframe')
    	iframe.style.opacity = 1
    });
})