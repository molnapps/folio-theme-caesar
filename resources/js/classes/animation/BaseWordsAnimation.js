export default class BaseWordsAnimation
{
	constructor(root, i) {
		this.root = root
		this.i = i
	}

	create(options = { baseDelay: 0, duration: 1000, offset: 100 }) {
		let baseDelay = options.baseDelay + (this.i * options.offset)

		let transition = this.getTransition()

		return new transition(this.root, '.word', {
			duration: options.duration,
			delay: function (el, i, l) {
				return baseDelay + (i * options.offset);
			}
		}).create()
	}

	getTransition() {
		throw new Error('Plase specify a transition class')
	}
}