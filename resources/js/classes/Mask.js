export default class Mask {
	constructor(selector) {
		this.elements = document.querySelectorAll(selector)
		this.options = {}
		this.block()
	}
	block() {
		this.options.className = 'mask'
		this.options.tagName = 'div'
		return this
	}
	inline() {
		this.options.className = 'mask--inline'
		this.options.tagName = 'span'
		return this
	}
	isInline() {
		return this.options.className == 'mask--inline'
	}
	create () {
		for (var i = 0; i < this.elements.length; i++) {
			var mask = this.createMaskDomElement()
			this.injectMaskInDom(mask, i)
			if (this.isInline()) {
				this.replaceWhiteSpacesWithFlexbox(mask, i)
			}
		}
	}
	createMaskDomElement() {
		var mask = document.createElement(this.options.tagName)
		mask.className = this.options.className
		return mask
	}
	injectMaskInDom(mask, i) {
		this.elements[i].parentNode.insertBefore(mask, this.elements[i])
		mask.appendChild(this.elements[i])
	}
	replaceWhiteSpacesWithFlexbox(mask, i) {
		mask.parentNode.style.display = 'flex'
		mask.parentNode.style.flexWrap = 'wrap'
		
		mask.style.marginRight = this.getWhiteSpaceInPixels(i) + 'px';
	}
	getWhiteSpaceInPixels(i) {
		var fontSize = this.getFontSize(i)
		return fontSize / 4
	}
	getFontSize(i) {
		var fontSize = window
			.getComputedStyle(this.elements[i], null)
			.getPropertyValue('font-size');

		return parseFloat(fontSize); 
	}
}