import BaseAnimation from './BaseAnimation'

export default class SlideUp extends BaseAnimation
{
	create() {
		this.animate({ translateY: ['100%', '0%'] })
	}
}