import BaseWordsAnimation from './BaseWordsAnimation'
import SlideUp from './SlideUp'

export default class WordsSlideUp extends BaseWordsAnimation
{
	getTransition() {
		return SlideUp
	}
}