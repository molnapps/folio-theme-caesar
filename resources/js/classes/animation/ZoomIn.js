import BaseAnimation from './BaseAnimation'

export default class ZoomIn extends BaseAnimation
{
	create() {
		this.animate({ scale: ['0%', '100%'] })
	}
}