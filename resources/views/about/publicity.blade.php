<div class="Section text-center py-16">
	<div class="mx-auto md:w-4/5 xl:w-2/3">
		<div class="flex justify-center">
			<h1 class="text-3xl pb-8 text-red-500 px-4 animate-words animate-words--slide-up">Selected Features &amp; Publicity</h1>
		</div>
		<ul class="Section__brands md:flex flex-wrap justify-start">
			@foreach (registry()->publicity()->all() as $publicity)
			<li class="About__card About__award md:w-1/3 px-4 flex flex-col justify-end items-center mb-4 py-16 border-gray-200">
				<span class="w-1/2 flex justify-center items-center mb-8">
					<img src="{{ $publicity['image'] }}" width="100" height="100" class="w-full">
				</span>
				<p class="flex-1 overflow-auto w-full flex flex-col">
					<span>{{ $publicity['title'] }}</span>
					<span>{{ $publicity['description'] }}</span>
					<span class="text-gray-500">
						{{ $publicity['city'] }}, {{ $publicity['year'] }}
					</span>
				</p>
			</li>
			@endforeach
		</ul>
	</div>
</div>