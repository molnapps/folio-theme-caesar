<?php

namespace Folio\Themes\Caesar;

use App\Themes\ThemeProvider as ThemeProviderInterface;

class ThemeProvider implements ThemeProviderInterface
{
	public function getBasePath()
	{
		return realpath(__DIR__ . '/..');
	}
}