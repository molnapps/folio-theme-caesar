<div class="bg-black h-16" data-js="header">
	<!-- Navbar -->
	<nav class="bg-black bg-opacity-80 backdrop-filter backdrop-blur fixed top-0 z-50 w-full max-h-screen overflow-y-scroll md:max-h-auto md:overflow-auto">
		<div class="mx-auto px-4 md:px-0 md:w-4/5 xl:w-2/3">
			<div class="flex justify-between">
				<div class="flex space-x-7 flex-1">
					<div>
						<!-- Website Logo -->
						<a href="/" class="flex items-center py-4">
							<img 
								src="{{ $settings->logo }}" 
								alt="{{ $settings->company }}" 
								class="h-8 w-8"
							>
							<span class="font-semibold text-gray-500 text-lg hidden">
								{{ $settings->company }}
							</span>
						</a>
					</div>
					<!-- Primary Navbar items -->
					<ul class="hidden md:flex items-center space-x-4">
						@foreach ($pages as $post)
						<li>
							<a 
								href="{{ $context->getPath($post) }}" 
								class="py-1 text-white font-semibold hover:text-red-500 transition duration-300"
							>{{ $post->title }}</a>
						</li>
						@endforeach
					</ul>
				</div>
				<!-- Contact button -->
				<div class="hidden md:flex items-center text-white align-right">
					<a 
						href="#" 
						class="py-1"
						data-js="contact-button" 
						data-js-close="Close &times;" 
						data-js-open="Contact"
					>Contact</a>
				</div>
				<!-- Mobile menu button -->
				<div class="md:hidden flex items-center">
					<button class="outline-none" data-js="mobile-menu-button">
						<svg class=" w-6 h-6 text-gray-500 hover:text-red-500"
							fill="none"
							stroke-linecap="round"
							stroke-linejoin="round"
							stroke-width="2"
							viewBox="0 0 24 24"
							stroke="currentColor"
						>
							<path d="M4 6h16M4 12h16M4 18h16"></path>
						</svg>
					</button>
				</div>
			</div>
		</div>
		<!-- Mobile menu -->
		<div class="hidden md:hidden" data-js="mobile-menu">
			<ul class="text-center py-2">
				@foreach ($pages as $post)
					<li class="px-4 py-2">
						<a 
							href="{{ $context->getPath($post) }}" 
							class="block text-sm p-2 text-white hover:bg-red-500 transition duration-300 border border-white rounded-full"
						>{{ $post->title }}</a>
					</li>
				@endforeach
			</ul>
		</div>
		<!-- Contacts form -->
		<div data-js="contact" class="hidden w-full bg-red-500 text-white">
			<div class="mx-auto p-4 md:px-0 md:w-4/5 xl:w-2/3 relative">
				<ol class="grid grid-cols-1 md:grid-cols-3 gap-4 pb-32 md:pb-0">
					@foreach ($links as $link)
						<li class="mb-0 md:mb-0 py-0 border-t border-red-200">
							<span class="flex flex-col">
								<strong class="uppercase text-xs text-red-200 pt-2">{{ $link->text }}</strong>
								<span class="py-1 md:py-0 md:pb-4">{{ $link->handle }}</span>
								<a 
									href="{{ $link->url }}" 
									class="text-xs md:text-base text-center md:text-left rounded-full border border-red-200 md:border-none text-red-200 hover:text-white p-1 md:p-0"
								>
										&rarr; {{ $link->cta }}
								</a>
							</span>
						</li>
					@endforeach
				</ol>
			</div>
		</div>
	</nav>
</div>