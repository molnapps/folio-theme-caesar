@extends('layout')
@section('contents')
<div class="bg-red-500">
	<div class="md:w-4/5 xl:w-2/3 mx-auto h-96 flex flex-col justify-center items-center text-center">
		<div class="mb-4">
			<a href="/">
				<img 
					src="{{ $settings->logo }}" 
					width="144" 
					height="144" 
					type="image/svg+xml" 
					codebase="http://www.adobe.com/svg/viewer/install" 
					style="filter: saturate(0) brightness(3);"
				/>
			</a>
		</div>
		<div class="texe-center">
			<p class="pb-4">
				<strong>Not found</strong><br/>
				The page you were looking for could not be found.<br/>
			</p>
			<a href="/"><strong>Back to the homepage</strong></a>
		</div>
	</div>
</div>
@endsection