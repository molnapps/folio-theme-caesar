import { SlideDown, ZoomIn, WordsSlideDown } from './../animation/all'

class ProjectAnimation
{
	constructor(root, i) {
		this.root = root
		this.i = i
	}

	create(options = { offset: 100 }) {
		let offset = this.i * options.offset
		
		this.animateProjectImage({ delay: offset })
		this.animateProjectTitle({ delay: offset + 200 })
		this.animateProjectClient({ delay: offset + 300 })
		this.animateProjectExcerpt({ delay: offset + 400})
	}

	animateProjectImage(override) {
		new SlideDown(this.root, '.Gallery__project-image', override).create()
		new ZoomIn(this.root, '.Gallery__project-logo-image', override).create()
	}

	animateProjectTitle(override) {
		new SlideDown(this.root, '.Gallery__project-title', override).create()
	}

	animateProjectClient(override) {
		new SlideDown(this.root, '.Gallery__project-client', override).create()
	}

	animateProjectExcerpt(override) {
		return new WordsSlideDown(
			this.root.querySelector('.Gallery__project-excerpt'), 
			this.i
		).create({ baseDelay: override.delay, duration: 500, offset: 10 })
	}
}

export default ProjectAnimation